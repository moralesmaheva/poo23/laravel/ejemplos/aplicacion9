<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\Models\Noticia;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NoticiaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //Auth es un facade de laravel (un helper)
        //Auth::user(); //devuelve el usuario autenticado
        //Auth::check(); //devuelve true si el usuario esta autenticado
        //Auth::guest(); //devuelve true si el usuario NO esta autenticado
        //Auth::user()->user_id; //devuelve el id del usuario
        //Auth::user()->name; //devuelve el nombre del usuario

        //obtengo todas las noticias
        $noticias = Noticia::paginate(5);
        //redireciono a la vista back.noticias.index
        return view('back.noticias.index', compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back.noticias.create', ['noticia' => new Noticia()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //subir la foto
        $fotoSubida = $request->file('foto');

        //almacenamos la foto
        $foto = $fotoSubida->store('fotos', 'public');

        //crear la noticia con fill y save
        $noticia = new Noticia();
        $noticia = $noticia->fill($request->all());
        //metemos el usuario autenticado
        $noticia->user_id = Auth::user()->id;
        $noticia->foto = $foto;
        $noticia->save();
        //redireccionar
        return redirect()->route('back.noticias.show', $noticia);
    }

    /**
     * Display the specified resource.
     */
    public function show(Noticia $noticia)
    {
        return view('back.noticias.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Noticia $noticia)
    {
        //comprobamos que el usuario es el dueño de la noticia
        if ($noticia->user_id != Auth::user()->id) {
            abort(403, 'No tienes permisos para editar esta noticia');
        }
        return view('back.noticias.edit', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Noticia $noticia)
    {
        //comprobamos si ha subido un fichero
        if ($request->hasFile('foto')) {

            //recupreamos la foto vieja
            $fotoSubida = $noticia->foto;
            //eliminamos la foto vieja
            Storage::disk('public')->delete($noticia->foto);
            //almaceno la nueva foto
            $fotoSubida = $request->file('foto')->store('fotos', 'public');
            //actualizo la noticia
            $noticia->fill($request->all());
            $noticia->foto = $fotoSubida;
        } else {
            //actualizo la noticia
            $noticia->fill($request->all());
        }
        $noticia->user_id = Auth::user()->id;
        $noticia->save();

        return redirect()->route('back.noticias.show', $noticia);
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Noticia $noticia)
    {
        //comprobamos que el usuario es el dueño de la noticia
        if ($noticia->user_id != Auth::user()->id) {
            abort(403, 'No tienes permisos para eliminar esta noticia');
        }
        //eliminamos la foto
        Storage::disk('public')->delete($noticia->foto);
        //eliminamos la noticia
        $noticia->delete();
        //redireccionamos
        return redirect()->route('back.noticias.index');
    }

    //para mostrar las noticias del usuario logueado     
    public function listado()
    {
        //obtengo las noticias del usuario logueado
        $noticias = Noticia::where('user_id', Auth::user()->id)->get();

        return view('back.noticias.listado', compact('noticias'));
    }
}
