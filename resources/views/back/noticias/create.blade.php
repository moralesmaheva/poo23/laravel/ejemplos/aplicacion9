<x-app-layout>
    <div>
        <div class="mb-4">
            <h1 class="text-white">Crear nueva noticia</h1>
        </div>
        <div class="mb-4">
            <form action="{{ route('back.noticias.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @include('components.mios._form')
            </form>
        </div>
    </div>
</x-app-layout>
