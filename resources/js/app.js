import './bootstrap';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();


//javascript para la previsualizacion de la imagen en el formulario
if (document.querySelector('#preview')) {

    document.querySelector('#fichero').addEventListener('change', (event) => {
        document.querySelector('#preview').src = window.URL.createObjectURL(event.target.files[0]);
    });
}

//eliminar un registro
//compruebamos que exista el boton
if (document.querySelector('#eliminar')) {
    // metemos javascript para el boton de borrar de la vista practica
    //con document.querySelector llegamos al id del boton de borrar
    //con .addEventListener escuchamos el evento submit del formulario
    document.querySelector('#eliminar').addEventListener('submit', (event) => {
        event.preventDefault(); //detengo el envio
        let confirmar = false;
        // mensaje emergente
        confirmar = window.confirm('¿Desea eliminar el registro?');
        if (confirmar) {
            document.querySelector('#eliminar').submit();
        }
    });
}

