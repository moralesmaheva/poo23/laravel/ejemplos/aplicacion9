<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoticiaController extends Controller
{

    /* 
    * funcion para listar las noticias
    * accesible para todos
    * no deben aparecer los botones de edicion
    * solo uno para ver la norticia completa (show)
    */
    public function index()
    {
        //obtengo todas las noticias
        $noticias = Noticia::paginate(5);
        //renderizo la vista en la vista front.index
        return view('front.index', compact('noticias'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Noticia $noticia)
    {
        return view('front.noticias.show', compact('noticia'));
    }

    public function buscar(Request $request)
    {
        $noticias = Noticia::where('titulo', 'like', '%' . $request->busqueda . '%')
            ->orWhere('contenido', 'like', '%' . $request->busqueda . '%')
            ->paginate(5);
        return view('front.index', compact('noticias'));
    }
}
