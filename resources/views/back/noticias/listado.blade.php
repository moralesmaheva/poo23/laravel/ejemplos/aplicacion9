<x-app-layout>
    @foreach ($noticias as $noticia)
        <div
            class="flex items-start gap-4 rounded-lg bg-white p-6 shadow-[0px_14px_34px_0px_rgba(0,0,0,0.08)] ring-1 ring-white/[0.05] transition duration-300 hover:text-black/70 hover:ring-black/20 focus:outline-none focus-visible:ring-[#FF2D20] lg:pb-10 dark:bg-zinc-900 dark:ring-zinc-800 dark:hover:text-white/70 dark:hover:ring-zinc-700 dark:focus-visible:ring-[#FF2D20]">
            <div class="flex size-12 shrink-0 items-center justify-center sm:size-16">
                <img src="{{ asset('storage/' . $noticia->foto) }}" alt="">
            </div>
            <div class="pt-3 sm:pt-5">
                <h2 class="text-xl font-semibold text-black dark:text-white">
                    {{-- mostrar el titulo --}}
                    {{ $noticia->titulo }}
                </h2>
                <p class="mt-4 text-sm/relaxed text-white">
                    {{-- mostrar los 100 primeros caracteres seguida de ... --}}
                    {{ Str::limit($noticia->contenido, 100, '...') }}
                </p>
                <div class="mt-4 text-blue-400 text-sm text-left">
                    {{-- mostrar el nombre del usuario i la fecha de actualizacion --}}
                    Creado por: {{ $noticia->user->name }}
                    <br>
                    Última actualización: {{ $noticia->updated_at->format('d/m/Y' . ' ' . 'H:i') }}
                </div>
            </div>
            <div class="mt-4 text-blue-400 text-right">
                <a href="{{ route('back.noticias.show', $noticia) }}" class="boton">Ver</a>
                <a href="{{ route('back.noticias.edit', $noticia) }}" class="boton">Editar</a>
                <form action="{{ route('back.noticias.destroy', $noticia) }}" method="post" id="eliminar">
                    @csrf
                    @method('delete')
                    <button type="submit" class="boton">Borrar</button>
                </form>
            </div>
        </div>
    @endforeach
</x-app-layout>
