<x-main-layout>
    <div>
        <form class="max-w-md mx-auto" action="{{ route('front.noticias.buscar') }}" method="POST">
            @csrf
            <label for="default-search"
                class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
            <div class="relative">
                <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                    </svg>
                </div>
                <input name="busqueda" type="search" id="default-search"
                    class="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-gray-500 focus:border-gray-500 dark:bg-gray-900 dark:border-gray-900 dark:placeholder-gray-400 dark:text-white dark:focus:ring-gray-500 dark:focus:border-gray-500"
                    placeholder="Buscar noticia ...." required />
                <button type="submit"
                    class="text-white absolute end-2.5 bottom-2.5 bg-white-700 hover:bg-white-800 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-800">{{ __('Search') }}</button>
            </div>
        </form>
    </div>
    
        @foreach ($noticias as $noticia)
            <a href="{{ route('front.noticias.show', $noticia) }}"
                class="flex items-start gap-4 rounded-lg bg-white p-6 shadow-[0px_14px_34px_0px_rgba(0,0,0,0.08)] ring-1 ring-white/[0.05] transition duration-300 hover:text-black/70 hover:ring-black/20 focus:outline-none focus-visible:ring-[#FF2D20] lg:pb-10 dark:bg-zinc-900 dark:ring-zinc-800 dark:hover:text-white/70 dark:hover:ring-zinc-700 dark:focus-visible:ring-[#FF2D20]">
                <div class="flex size-12 shrink-0 items-center justify-center rounded-full bg-[#FF2D20]/10 sm:size-16">
                    <img src="{{ asset('storage/fotos/' . 'images.jpg') }}" alt="">
                </div>

                <div class="pt-3 sm:pt-5">
                    <h2 class="text-xl font-semibold text-black dark:text-white">
                        {{-- mostrar el titulo --}}
                        {{ $noticia->titulo }}
                    </h2>

                    <p class="mt-4 text-sm/relaxed">
                        {{-- mostrar los 100 primeros caracteres seguida de ... --}}
                        {{ Str::limit($noticia->contenido, 100, '...') }}
                    </p>
                    <div class="mt-4 text-blue-400 text-sm text-right">
                        {{-- mostrar el nombre del usuario i la fecha de actualizacion --}}
                        Creado por: {{ $noticia->user->name }}
                        <br>
                        Última actualización: {{ $noticia->updated_at->format('d/m/Y') }}
                    </div>
                </div>
            </a>
        @endforeach
    </div>
    <div>
        {{ $noticias->links() }}
    </div>
</x-main-layout>
