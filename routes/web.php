<?php

use App\Http\Controllers\back\NoticiaController as BackNoticiaController;
use App\Http\Controllers\front\NoticiaController as FrontNoticiaController;
use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;



//controlador de recursos que pueden ver todos los usuarios
//Route::resource('noticias', NoticiaController::class);

//se pone antes de las de recursos para que no salte el middleware
//si lo pongo debajo sale la autenticacion
//ruta pagina de inicio del front
//muestra algunos campos de las noticias y para pinchar para verla completa
//quiero que salga en el menu Panel, login y registrar

Route::controller(FrontNoticiaController::class)->group(function () {
    //muestra todas las noticias
    Route::get('/', 'index')->name('home');
    //muestra una sola noticia
    Route::get('front/noticias/{noticia}', 'show')->name('front.noticias.show');
    //ruta para el buscador
    Route::post('front/noticias/buscar', 'buscar')->name('front.noticias.buscar');
});

//los usuarios autenticados pueden acceder a estas rutas
Route::middleware('auth')->group(function () {
    //pagina de inicio del back
    //quiero ver la informacion del usuario
    //si pincho en el icono del menu me lleva al front
    //que muestre en el menu Gestion de noticias, crear noticia, noticias de usuartio y editar noticia
    //un dropdown para perfil o cerrar sesion
    Route::get('/back/index', function () {
        return view('back.index');
    })->name('back.index');

    Route::get('/profile', [ProfileController::class, 'edit'])
        ->name('profile.edit');

    Route::patch('/profile', [ProfileController::class, 'update'])
        ->name('profile.update');

    Route::delete('/profile', [ProfileController::class, 'destroy'])
        ->name('profile.destroy');

    //para mostrar las noticias del usuario logueado
    Route::controller(BackNoticiaController::class)->group(function () {
        Route::get('/back/noticias/listado', 'listado')->name('back.noticias.listado');
    });

    //para la gestion de las noticias
    Route::resource('back/noticias', BackNoticiaController::class)
        ->names('back.noticias');
});



require __DIR__ . '/auth.php';
