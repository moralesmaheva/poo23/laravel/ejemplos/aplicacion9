<div class="align-middle inline-block min-w-full sm:px-6 lg:px-8 text-white">
    <div class="mb-4">
        <label class="block text-sm font-medium leading-6 text-gray-900 text-white" for="titulo">Título de la
            noticia</label>
        <input class="text-black" type="text" name="titulo" id="titulo" value="{{ old('titulo', $noticia->titulo) }}">
        @error('titulo')
            <div class="error">{{ $message }}</div>
        @enderror
    </div>
    <div class="border-b border-gray-900/10 pb-12">
        <label class="block text-sm font-medium leading-6 text-gray-900 text-white" for="contenido">Contenido de la
            noticia</label>
        <textarea rows="4"
            class="block p-2.5 w-full text-sm text-gray-900 bg-white"
            name="contenido" id="contenido" value="{{ old('contenido', $noticia->contenido) }}"></textarea>
        @error('contenido')
            <div class="error">{{ $message }}</div>
        @enderror
    </div>
    <div class="border-b border-gray-900/10 pb-12">
        <label class="text-white" for="foto">Foto</label>
        @if ($noticia->foto)
            <img src="{{ asset('storage/' . $noticia->foto) }}" id="preview">
        @else
            <img src="" id="preview">
        @endif
        <input type="file" name="foto" id="foto" value="{{ $noticia->foto }}" id="fichero">
        @error('foto')
            <div class="error">{{ $message }}</div>
        @enderror
    </div>
    <div class="mt-6 flex items-center justify-end gap-x-6">
        <button class=" button-gray text-white" type="submit" class="boton">Guardar</button>
    </div>
</div>
