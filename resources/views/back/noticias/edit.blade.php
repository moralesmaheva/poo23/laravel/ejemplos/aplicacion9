<x-app-layout>
    <div>
        <h1>Actualizar noticia</h1>
    </div>
    <div class="tarjeta">
        <form action="{{ route('back.noticias.update',$noticia) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @include('components.mios._form')
        </form>
    </div>   
</x-app-layout>