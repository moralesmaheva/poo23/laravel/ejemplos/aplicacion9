<x-app-layout>
    {{-- ponemos la imagen de la noticia encima del texto --}}
    <div class="mt-4 flex size-60 shrink-0 items-center justify-center bg-[#FF2D20]/10 ">
        <img src="{{ asset('storage/' . $noticia->foto) }}" alt="">
    </div>
    <div class="pt-3 sm:pt-5">
        <h2 class="text-xl font-semibold text-black dark:text-white">
            {{-- mostrar el titulo --}}
            {{ $noticia->titulo }}
        </h2>
        <p class="mt-4 text-sm/relaxed text-white">
            {{-- mostrar los 100 primeros caracteres seguida de ... --}}
            {{ $noticia->contenido }}
        </p>
        <div class="mt-4 text-sm text-center">
            {{-- mostrar el nombre del usuario i la fecha de actualizacion --}}
            <div class="text-blue-400 capitalize">Creado por:{{ $noticia->user->name }}</div>
            <div class="text-blue-400">Fecha de creación: {{ $noticia->created_at->format('d/m/Y') }}</div>
            <div class="text-blue-400">Fecha de actualización {{ $noticia->updated_at->format('d/m/Y') }}</div>
        </div>
        <div class="mt-4 text-blue-400">
            <a href="{{ route('back.noticias.edit', $noticia) }}" class="boton">Editar</a>
            <form action="{{ route('back.noticias.destroy', $noticia) }}" method="post" id="eliminar">
                @csrf
                @method('delete')
                <button type="submit" class="boton">Borrar</button>
            </form>
        </div>
    </div>
</x-app-layout>