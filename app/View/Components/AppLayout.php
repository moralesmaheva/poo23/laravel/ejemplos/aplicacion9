<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class AppLayout extends Component
{
    /**
     * Este componente lo utilizamos como layaut para las vistas del back
     */
    public function render(): View
    {
        return view('layouts.app');
    }
}
