<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Noticia extends Model
{
    use HasFactory;

    //nombre de la tabla
    protected $table = 'noticias';

    //campos para la asignacion masiva
    protected $fillable = [
        'user_id',
        'titulo',
        'contenido',
        'foto',
    ];
    

    //relaciona la noticia creada con el id del usuario
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
