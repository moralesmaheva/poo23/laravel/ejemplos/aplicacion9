<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class GuestLayout extends Component
{
    /**
     * Este componente es el layoutr para las vistas de registro y login
     */
    public function render(): View
    {
        return view('layouts.guest');
    }
}
